class CreateGoods < ActiveRecord::Migration
  def change
    create_table :goods do |t|
      t.string :name
      t.text :describe
      t.float :price
      t.references :merchant, index: true

      t.timestamps
    end
  end
end
