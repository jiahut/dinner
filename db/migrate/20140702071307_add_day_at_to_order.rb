class AddDayAtToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :day_at, :date
  end
end
