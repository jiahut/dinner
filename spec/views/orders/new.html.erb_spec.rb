require 'rails_helper'

RSpec.describe "orders/new", :type => :view do
  before(:each) do
    assign(:order, Order.new(
      :user => "",
      :product => ""
    ))
  end

  it "renders new order form" do
    render

    assert_select "form[action=?][method=?]", orders_path, "post" do

      assert_select "input#order_user[name=?]", "order[user]"

      assert_select "input#order_product[name=?]", "order[product]"
    end
  end
end
