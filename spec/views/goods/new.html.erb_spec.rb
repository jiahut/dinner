require 'rails_helper'

RSpec.describe "goods/new", :type => :view do
  before(:each) do
    assign(:good, Good.new(
      :name => "MyString",
      :describe => "MyText",
      :price => 1.5,
      :merchant => nil
    ))
  end

  it "renders new good form" do
    render

    assert_select "form[action=?][method=?]", goods_path, "post" do

      assert_select "input#good_name[name=?]", "good[name]"

      assert_select "textarea#good_describe[name=?]", "good[describe]"

      assert_select "input#good_price[name=?]", "good[price]"

      assert_select "input#good_merchant_id[name=?]", "good[merchant_id]"
    end
  end
end
