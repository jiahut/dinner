require 'rails_helper'

RSpec.describe "goods/index", :type => :view do
  before(:each) do
    assign(:goods, [
      Good.create!(
        :name => "Name",
        :describe => "MyText",
        :price => 1.5,
        :merchant => nil
      ),
      Good.create!(
        :name => "Name",
        :describe => "MyText",
        :price => 1.5,
        :merchant => nil
      )
    ])
  end

  it "renders a list of goods" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
