require 'rails_helper'

RSpec.describe "goods/show", :type => :view do
  before(:each) do
    @good = assign(:good, Good.create!(
      :name => "Name",
      :describe => "MyText",
      :price => 1.5,
      :merchant => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(//)
  end
end
