class ConsoleController < ApplicationController
  def index
    @goods = Good.includes(:merchant).joins(:merchant).all
    @merchants = Merchant.all
    @order_items = Order.select(:merchant_id,:good_id).where(day_at: Date.current).joins(:good).references(:good).group_by(&:merchant_id)
    # @order_items = Good.where(id: Order.select(:good_id).where(day_at: Date.current)).group_by(&:merchant_id)
  end
end
