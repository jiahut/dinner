json.array!(@goods) do |good|
  json.extract! good, :id, :name, :describe, :price, :merchant_id
  json.url good_url(good, format: :json)
end
