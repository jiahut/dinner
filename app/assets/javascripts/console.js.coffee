# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $(".dinner-shop-name>ul").on "click","li", (e)->
    $self = $(e.currentTarget)
    mid = $self.data("merchant-id")
    $self.addClass("active")
    $self.siblings().removeClass("active")
    $(".dinner-goods>tbody>tr").each (idx)->
      $(this).removeClass("hide")
      if $(this).data("merchant-id") != mid
        $(this).addClass("hide")

  $(".dinner-shop-name li:first").trigger("click")
