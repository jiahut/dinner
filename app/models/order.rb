class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :good

  scope :merchant, -> { Merchant.where(id: good.merchant_id) }
end
