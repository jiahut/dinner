class Good < ActiveRecord::Base
  belongs_to :merchant
  has_many :orders
end
