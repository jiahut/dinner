module ConsoleHelper
  def show_merchanrt_name(id)
    Merchant.find(id).name
  end

  def show_goods_name(orders)
    orders.map(&:good_id).map { |id| Good.find(id).name }.join(",")
  end

  def order?
    Order.where(day_at: Date.current).where(user_id: current_user).present?
  end
end
