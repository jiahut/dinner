class UserMailer < ActionMailer::Base
  default from: "zhijia@upsmart.com"

  def welcome_email(user)
    @user = user
    @url = 'dinner.unionpaysmart.com'
    mail to: @user.email, subject: "welcom to dinner site"
  end
end
